<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

// use App\Fibonaci;
use Illuminate\Http\Request;

class FibonacisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        return view('fibonacis.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
      return view('fibonacis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
      $requestData = $request->all();
      $rules = [
        'angka' => 'required|numeric|min:1',
      ];
      $nicename = array(
        'angka' => "Angka",
      );
      $messages = [
        'required' => ':attribute harus diisi',
        'min' => ':attribute minimal  :min .',
        'max' => ':attribute maksimal  :max karakter.',
        'numeric' => ':attribute harus berupa angka',
        'unique' => ':attribute sudah ada sebelumnya.',
        'required_if' => ':attribute harus diisi jika :other bernilai :value',
      ];
      $validator = \Validator::make($request->all(), $rules, $messages, $nicename);
      if ($validator->fails()) {
        return redirect()->back()->with('errors', $validator->errors())
          ->withInput();
      }
      $hasil = $this->segitiga($requestData['angka']);
      return redirect()->back()->with('fibonaci', $hasil)
        ->withInput();
      // Fibonaci::create($requestData);
      // return redirect('fibonacis')->with('flash_message', 'Fibonaci added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $fibonaci = Fibonaci::findOrFail($id);

        return view('fibonacis.show', compact('fibonaci'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $fibonaci = Fibonaci::findOrFail($id);

        return view('fibonacis.edit', compact('fibonaci'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $fibonaci = Fibonaci::findOrFail($id);
        $fibonaci->update($requestData);

        return redirect('fibonacis')->with('flash_message', 'Fibonaci updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Fibonaci::destroy($id);
        return redirect('fibonacis')->with('flash_message', 'Fibonaci deleted!');
    }
    function fibonacci($total)
    { $before = 0;
      $current = 1;
      $result = $current;
      for($i = 1;$i<$total;$i++)
      { $output = $current + $before;
        $result .= ' '.$output;
        $before = $current;
        $current = $output;
      } return $result;
    }
    function segitiga($total)
    {
      $a =1;
      $param ='';
      while ($a <= $total) {
        $param .= 'Iterasi-'.$a.': '.$this->fibonacci($a).'</br>';
        $a++;
      }
      return $param;
    }
}
