<div class="form-group {{ $errors->has('angka') ? 'has-error' : ''}}">
    <label for="nama" class="control-label">{{ 'Angka :' }}</label>
    <input class="form-control" autocomplete="off" min="1" name="angka" type="number" id="angka" value="{{isset($satuan)?old('angka', $satuan->nama):old('angka')}}" >
    {!! $errors->first('angka', '<p class="help-block">:message</p>') !!}
</div>
@if(Session::has('fibonaci'))
  <div class="form-group ">
    <label for="nama" class="control-label bold">{{ 'Hasil Fibonaci :' }}</label>
    </br>
    <label for="form-control">{!! Session('fibonaci') !!}</label>
  </div>
@endif
<div class="form-group">
    <div class="col-lg-2 ">
        <a href="{{url('fibonacis')}}" class="btn btn-social-icon btn-success"><i class="icon-loop3"></i></a>
    </div>
    <div class="col-lg-10">
        <button id="btn_simpan" type="submit" class="btn btn-social-icon btn-primary pull-right"><i class=" icon-floppy-disk"></i> </button>
    </div>
</div>
