@extends('master.masterbackend')
@section('header_bold','')
@section('header_semibold',' Beranda')
@section('header_small',' Fibonaci')
@section('master','active')
@section('header_icon')
<i class="icon-display4 position-left"></i>
@endsection
@section('heading_btn')
@endsection
@section('breadcrumb')
<li><a href=""><i class="icon-display4 position-left"></i>Beranda</a></li>
<li> Fibonaci </li>
@endsection
@section('csstambahan')
@endsection
@section('isi')
<div class="page-container">
    {{--<!-- Page content -->--}}
    <div class="page-content">
        {{--<!-- Main content -->--}}
        <div class="content-wrapper">
            <div class="panel panel-flat border-success">
                <div class="panel-heading">
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                  <form method="POST" action="{{ url('/fibonacis') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                      {{ csrf_field() }}
                      @include ('fibonacis.form', ['formMode' => 'create'])
                  </form>
                </div>
            </div>
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->
</div>
<!-- /page container -->
@endsection
@push('scripttambahan')
<script type="text/javascript">
$(document).ready(function() {
{{--/* =========== BEGIN AJAX HEADER SECTION ================ */--}}
$.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
{{--/* =========== END AJAX HEADER SECTION ================ */--}}
});
$('.dataTables_length select').select2({
  minimumResultsForSearch: Infinity,
  width: 'auto'
});
</script>
@endpush
