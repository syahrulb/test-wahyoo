{{--<!-- Page header -->--}}
<div class="page-header">
	<div class="breadcrumb-line">
			<ul class="breadcrumb">
				@yield('breadcrumb')
			</ul>
			<ul class="breadcrumb-elements">
				<li class="@yield('beranda')"><a href="{{ url('/') }}"><i class="icon-display4 position-left"></i> Beranda </a></li>
				<!-- <li class="@yield('Transaksi')"><a href="{{ url('/nota-pengadaan-atks') }}"><i class="icon-transmission position-left"></i>Transaksi</a></li>
				<li class="@yield('master')"><a href="{{ url('/kategori-barangs') }}"><i class="icon-newspaper position-left"></i>Master</a></li> -->
			</ul>
		</div>
	<div class="page-header-content">
		<div class="page-title">
				<h4>@yield('header_icon') <span class="text-semibold">@yield('header_bold')</span> @yield('header_semibold') <small>@yield('header_small')</small></h4>
			</div>

    <div class="heading-elements">
			<div class="heading-btn-group">
	      @yield('heading_btn')
			</div>
		</div>
	</div>
</div>
{{--<!-- /page header -->--}}
