{{--<!-- Main navbar -->--}}
<div class="navbar navbar-inverse bg-indigo-800">
  <div class="navbar-header">
    <a class="navbar-brand" href="{{ url('/home') }}"><img src="{{ asset('assets/images/logo_icon_light.png') }}" alt=""></a>
    <ul class="nav navbar-nav pull-right  @if(URL::current() == URL::to('/')) visible-xs-block @endif ">
      {{--<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>--}}
				<li>
					<a class="sidebar-control sidebar-main-toggle hidden-xs">
						<i class="icon-paragraph-justify3"></i>
					</a>
        </li>
    </ul>
    <h class="navbar-text"></h1>
  </div>
  <div class="navbar-collapse collapse" id="navbar-mobile">
    <ul class="nav navbar-nav navbar-right">
      <li class="dropdown dropdown-user">
        <a class="dropdown-toggle" data-toggle="dropdown">
          <img src="{{ asset('img/imgmasterbackend/logo.png') }}" alt="">
          <span>Guest</span>
          <i class="caret"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
          <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
          <li class="divider"></li>
          <li>
            <a href="{{ url('/logout') }}" onclick="event.preventDefault(); "><i class="icon-switch2"></i> Keluar</a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</div>
{{--<!-- /main navbar -->--}}
