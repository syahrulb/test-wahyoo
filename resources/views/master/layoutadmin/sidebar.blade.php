{{--<!-- Main sidebar --> --}}
<div class="sidebar sidebar-main sidebar-default">
	<div class="sidebar-fixed">
		<div class="sidebar-content">
			{{--<!-- Main navigation -->--}}
			<div class="sidebar-category sidebar-category-visible">
				<div class="category-title h6">
					<span>{{ explode('.',Route::current()->getName())[0] }}</span>
					<ul class="icons-list">
						<li><a href="#" data-action="collapse"></a></li>
					</ul>
				</div>
				<div class="category-content no-padding">
					<ul class="navigation navigation-main navigation-accordion">
						{{--<!-- Main --> --}}
						@if(explode('.',Route::current()->getName())[0] == 'Master')
						<li class="navigation-header"><span>Master</span> <i class="icon-menu" title="Main pages"></i></li>
						<li class="{{(explode('.',Route::current()->getName())[1] == 'kategori-barangs')?'active':''}}"><a href="{{ url('/kategori-barangs') }}"><i class="fa fa-paperclip"></i> <span>Kategori Barang</span></a></li>
						<li class="{{(explode('.',Route::current()->getName())[1] == 'atks')?'active':''}}"><a href="{{ url('/atks') }}"><i class="icon-pencil3"></i> <span>ATK</span></a></li>
						<li class="{{(explode('.',Route::current()->getName())[1] == 'satuans')?'active':''}}"><a href="{{ url('/satuans') }}"><i class="icon-dropbox"></i> <span>Satuan</span></a></li>
						<li class="{{(explode('.',Route::current()->getName())[1] == 'rule-users')?'active':''}}"><a href="{{ url('/rule-users') }}"><i class="fa fa-users"></i> <span>Rule User</span></a></li>
						<li class="{{(explode('.',Route::current()->getName())[1] == 'users')?'active':''}}"><a href="{{ url('/users') }}"><i class="icon-user"></i> <span>User</span></a></li>
						<li class="{{(explode('.',Route::current()->getName())[1] == 'pegawais')?'active':''}}"><a href="{{ url('/pegawais') }}"><i class="icon-users"></i> <span>Pegawai</span></a></li>
						<li class="{{(explode('.',Route::current()->getName())[1] == 'bidangs')?'active':''}}"><a href="{{ url('/bidangs') }}"><i class="icon-city"></i> <span>Bidang</span></a></li>
						<li class="{{(explode('.',Route::current()->getName())[1] == 'sub-bidangs')?'active':''}}"><a href="{{ url('/sub-bidangs') }}"><i class="icon-office"></i> <span>SubBidang</span></a></li>
						<li class="{{(explode('.',Route::current()->getName())[1] == 'penyimpanans')?'active':''}}"><a href="{{ url('/penyimpanans') }}"><i class="icon-basket"></i> <span>Penyimpanan</span></a></li>
						@elseif(explode('.',Route::current()->getName())[0] == 'Transaksi')
						<li class="navigation-header"><span>ATK</span> <i class="icon-menu" title="" data-original-title="Main pages"></i></li>
						<li class="{{(explode('.',Route::current()->getName())[1] == 'nota-pengadaan-atks')?'active':''}}"><a href="{{ url('/nota-pengadaan-atks') }}"><i class="icon-basket"></i> <span>Nota Pengadaan Atk </span></a></li>
						<li class="{{(explode('.',Route::current()->getName())[1] == 'nota-permintaan-atks')?'active':''}}"><a href="{{ url('/nota-permintaan-atks') }}"><i class="icon-cart2"></i> <span>Nota Permintaan Atk </span></a></li>
						@else
						@endif
						{{--<!-- /main -->--}}
					</ul>
				</div>
			</div>
			{{--<!-- /main navigation --> --}}
		</div>
	</div>
</div>
{{--<!-- /main sidebar --> --}}
