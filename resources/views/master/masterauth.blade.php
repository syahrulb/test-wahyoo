<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ config('app.name') }}</title>

	{{--<!-- Global stylesheets -->---}}
	<link href="{{asset('assets/css/roboto.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/colors.css')}}" rel="stylesheet" type="text/css">
	{{--<!-- /global stylesheets -->--}}

	{{--<!-- Core JS files -->--}}
	<script type="text/javascript" src="{{asset('assets/js/plugins/loaders/pace.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/core/libraries/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/loaders/blockui.min.js')}}"></script>
	{{--<!-- /core JS files -->--}}

  {{--<!-- Sweetalert Css -->--}}
  <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" />

	{{--<!-- Theme JS files -->--}}
	<script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

	<script type="text/javascript" src="{{asset('assets/js/core/app.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/pages/login.js')}}"></script>
	{{--<!-- /theme JS files -->--}}
	@yield('csstambahan')

</head>

<body class="login-container">

	{{--<!-- Page container -->--}}
	<div class="page-container">

		{{--<!-- Page content -->--}}
		<div class="page-content">

			{{--<!-- Main content -->--}}
			<div class="content-wrapper">

				{{--<!-- Content area -->--}}
				<div class="content pb-20">

					{{--<!-- Advanced login -->--}}
						@yield('content')
					{{--<!-- /advanced login -->--}}

				</div>
				{{--<!-- /content area -->--}}

			</div>
			{{--<!-- /main content -->--}}

		</div>
		{{--<!-- /page content -->--}}

	</div>
	{{--<!-- /page container -->--}}

  {{--<!-- SweetAlert Plugin Js -->--}}
  <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>

	@yield('scripttambahan')
  @include('sweet::alert')

	@if (Session::has('berhasil'))
	<script>swal("Success!", "{{ Session('berhasil') }}", "success");</script>
	@elseif(Session::has('gagal'))
	<script>swal("Success!", "{{ Session('gagal') }}", "success");</script>
	@elseif(Session::has('peringatan'))
	<script>swal("Peringatan!", "{{ Session('peringatan') }}", "success");</script>
	@elseif(Session::has('informasi'))
	<script>swal("Informasi!", "{{ Session('informasi') }}", "warning");</script>
	@endif
</body>
</html>
