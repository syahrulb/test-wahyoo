<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="author" content="@bappeko | Syahrul Bastomy">
  <meta name="description" content="Website untuk musyawarah bagi dinas kota surabaya">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="@bappeko">
  <meta name="twitter:creator" content="@bappeko">
  <meta name="twitter:title" content="SiMBa - Sistem Informasi Bangunan">
  <meta name="twitter:description" content="Website mengetahui kondisi bangunan dinas kota surabaya">
  <meta name="twitter:url" content="https://bappeko.surabaya.go.id/inventaris">
	<title>@yield('judulweb')</title>
  <link rel="icon" href="{{asset('img/imgmasterbackend/logo.png')}}" type="image/x-icon">

	{{--<!-- Global stylesheets -->--}}
	<link href="{{asset('css/formWizard.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/roboto.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/colors.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('assets/css/extras/animate.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('plugins/lightbox/lightbox.css')}}" rel="stylesheet" type="text/css">
	{{--<!-- <link href="{{asset('assets/js/plugins/pickers/datepicker.css')}}" rel="stylesheet" type="text/css"> -->--}}

  {{--<!-- fancybox style -->--}}
  <link rel="stylesheet" href="{{asset('plugins/fancybox/jquery.fancybox.min.css')}}">
  {{--<!-- Font Awesome Css-->--}}
  <link href="{{asset('plugins/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
  {{--<!-- Sweetalert Css -->--}}
  <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" />
  {{--<!-- progressbar step -->--}}
  <link href="{{asset('plugins/progressbar/progress-wizard.min.css')}}" rel="stylesheet">
  {{--<!-- Waves Effect Css -->--}}
  <link href="{{asset('plugins/node-waves/waves.css')}}" rel="stylesheet" />

	{{--<!-- /global stylesheets -->--}}
  @yield('csstambahan')
</head>

<body>

  {{--<!-- Main navbar -->--}}
      @include('master.layoutadmin.navbar')
  {{--<!-- /main navbar -->--}}

	{{--<!-- Header -->--}}
      @include('master.layoutadmin.header')
	{{--<!-- Header -->--}}

	{{--<!-- Page container -->--}}
    @yield('isi')
	{{--<!-- /page container -->--}}

  {{--<!-- Footer -->--}}
      @include('master.layoutadmin.footer')
  {{--<!-- Footer -->--}}

  {{--<!-- Core JS files -->--}}
  <script type="text/javascript" src="{{asset('assets/js/plugins/loaders/pace.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/core/libraries/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/core/libraries/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/plugins/loaders/blockui.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/plugins/ui/nicescroll.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/plugins/ui/drilldown.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/editable-table/mindmup-editabletable2.js')}}"></script>
  {{--<!-- /core JS files -->--}}

  {{--<!-- Theme JS files -->--}}
	<script type="text/javascript" src="{{asset('assets/js/core/libraries/jquery_ui/core.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/forms/selects/selectboxit.min.js')}}"></script>
	{{--<!-- <script type="text/javascript" src="{{asset('assets/js/plugins/forms/selects/bootstrap_select.min.js')}}"></script> -->--}}
	<script type="text/javascript" src="{{asset('assets/js/plugins/notifications/pnotify.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/notifications/noty.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/notifications/jgrowl.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/forms/selects/selectboxit.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/forms/selects/bootstrap_select.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/pickers/datepicker.js')}}"></script>
  <script type="text/javascript" src="{{asset('/assets/js/plugins/uploaders/fileinput.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('/plugins/lightbox/lightbox.js')}}"></script>

	<script type="text/javascript" src="{{asset('assets/js/core/libraries/jquery_ui/interactions.min.js')}}"></script>
  {{--<!-- SweetAlert Plugin Js -->--}}
  <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
  {{--<!-- Waves Effect Plugin Js -->--}}
  <script src="{{asset('plugins/node-waves/waves.js')}}"></script>
  {{--<!-- fancybox Plugin Js -->--}}
  <script type="text/javascript" src="{{asset('plugins/fancybox/jquery.fancybox.min.js')}}"></script>


  <script type="text/javascript" src="{{asset('assets/js/core/app.js')}}"></script>
  {{--<!-- <script type="text/javascript" src="assets/js/pages/datatables_basic.js"></script> -->--}}
  {{--<!-- /theme JS files --> --}}

  <script type="text/javascript">
    $(document).ready(function() {
    {{--/* =========== BEGIN AJAX HEADER SECTION ================ */--}}
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    {{--/* =========== END AJAX HEADER SECTION ================ */--}}
    });
    {{--/* =========== START STATIC VAR SECTION ================ */--}}
      $('.bootstrap-select').select2({
        minimumResultsForSearch: Infinity
      });
      // Default initialization
      $(".styled, .multiselect-container input").uniform({
          radioClass: 'choice'
      });
    {{--/* =========== END STATIC VAR SECTION ================ */--}}
  </script>
  <script type="text/javascript">
  function block_div(val) {
    $(val).block({
        message: "<div class='pace-demo' style='z-index: +1'><div class='theme_squares'><div class='pace_progress' data-progress-text='60%' data-progress='60'></div><div class='pace_activity'></div></div></div>",
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.95,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            margin: 0,
            backgroundColor: 'none',
            color: '#fff'
        }
    });
  }
  function unblock_div(val) {
    window.setTimeout(function () {
        $(val).unblock();
    }, 100);
  }
  </script>
  @stack('scripttambahan')
  @stack('script')

	@if (Session::has('berhasil'))
	<script>swal("Success!", "{{ Session('berhasil') }}", "success");</script>
	@elseif(Session::has('gagal'))
	<script>swal("Gagal!", "{{ Session('gagal') }}", "error");</script>
	@elseif(Session::has('peringatan'))
	<script>swal("Peringatan!", "{{ Session('peringatan') }}", "warning");</script>
	@elseif(Session::has('informasi'))
	<script>swal("Informasi!", "{{ Session('informasi') }}", "info");</script>
	@endif
</body>
</html>
